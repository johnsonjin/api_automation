package SwychMoney;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import Utilities.Constants;
import Utilities.DataProviderParameter;
import Utilities.TestBase;
import io.restassured.path.json.JsonPath;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Loyalty_Stellar extends TestBase {
	
	 /*
	 String clinet_id = "swych_b2b_test";
	 String accountid = "3199989005101011";
	 String ApiKey = "swych_b2b_test";
	 */
	 String orderId =""; 
	 String token = "";
	 String voucherCode = "";
	 String txn_id = "";
	 String accessCode = "";
	
	 @DataProviderParameter("FileName=" + Constants.testData_createorder_2 + ";Sheet=Sheet1")	
	 @Test(priority=1,dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
	 public void put_createorder(String URI, String clientId, String apiKey, String OcpApimSubscriptionKey,
			String accountId, String brandId, String productId, String amount, String senderFirstName ,String senderLastName, String senderEmail, String senderPhone, String recipientFirstName, 
			String recipientLastName, String recipientEmail, String recipientPhoneNumber, String notificationDeliveryMethod, String giftDeliveryMode, String swychable, String deliverGiftTo) throws JSONException 
	 {  
		 	RestAssured.baseURI = URI;
		    RequestSpecification request = RestAssured.given();
		    System.out.println("\nNow Creating Order " );
		    request.header("Content-Type", "application/json").
		    header("clientId", clientId).
		    header("apiKey", apiKey).
		    header("Ocp-Apim-Subscription-Key", OcpApimSubscriptionKey);
		    
		    JSONObject requestParams = new JSONObject();		    
		    requestParams.put("accountId", accountId);
		    requestParams.put("brandId", brandId);
	    	requestParams.put("productId", productId);
	    	requestParams.put("amount", amount);
	    	requestParams.put("senderFirstName", senderFirstName);
			requestParams.put("senderLastName", senderLastName);
	    	requestParams.put("recipientFirstName", recipientFirstName);
			requestParams.put("recipientLastName", recipientLastName);
		    requestParams.put("senderEmail", senderEmail);
		    requestParams.put("senderPhone", senderPhone);
	    	requestParams.put("recipientEmail", recipientEmail);
	    	requestParams.put("recipientPhoneNumber", recipientPhoneNumber);
	    	requestParams.put("notificationDeliveryMethod", notificationDeliveryMethod);
		    requestParams.put("giftDeliveryMode", giftDeliveryMode);
	    	requestParams.put("swychable", swychable);
	    	requestParams.put("deliverGiftTo", deliverGiftTo);	    	
	    
	    	request.body(requestParams.toString());
			 
			Response response = request.put(URI + "/api/v1/b2b/order");
					 
		    int statusCode = response.getStatusCode();
		    //Assert that correct status code is returned.
		    Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
		    
			String responsebody = response.getBody().asString();
			JSONObject json = new JSONObject(responsebody);
			System.out.println("The response from create order is:\n" + json.toString(10));
				 
			JsonPath jsonPathEvaluator = response.jsonPath();
			int status = jsonPathEvaluator.get("statusCode");
			Assert.assertEquals(status, 16010900);
			
			orderId = jsonPathEvaluator.get("orderId");
			System.out.println("The order id is " + orderId);	 
			
			accessCode = jsonPathEvaluator.get("AccessCode");
			System.out.println("The accessCode is " + accessCode);	 
 }
	
	 @DataProviderParameter("FileName=" + Constants.testData_confirmorder + ";Sheet=Sheet1")	
	 @Test(priority=2,dependsOnMethods = {"put_createorder"},dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
	 public void post_confirmorder(String URI, String clientId, String apiKey, String OcpApimSubscriptionKey, String accountId) throws JSONException 
	 {		 
		 	RestAssured.baseURI = URI;
		    RequestSpecification request = RestAssured.given();
		    System.out.println("\nNow Confirming Order " );
		    request.header("Content-Type", "application/json").
		    header("clientId", clientId).
		    header("apiKey", apiKey).
		    header("Ocp-Apim-Subscription-Key", OcpApimSubscriptionKey);
		    
		    JSONObject requestParams = new JSONObject();		    
		    requestParams.put("accountId", accountId);
		    requestParams.put("orderId", orderId);
		    requestParams.put("accessCode", accessCode);
	    				
	    	request.body(requestParams.toString());			 
			
			Response response = request.post(URI + "/api/v1/b2b/order");
					 
		    int statusCode = response.getStatusCode();
		    //Assert that correct status code is returned.
		    Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
		    
		    String responsebody = response.getBody().asString();
			JSONObject json = new JSONObject(responsebody);
			System.out.println("The response from confirm order is:\n" + json.toString(10));
				 
			JsonPath jsonPathEvaluator = response.jsonPath();
			int status= jsonPathEvaluator.get("statusCode");
			Assert.assertEquals(status, 16011000);
			
			
		    JSONObject obj1 = new JSONObject(responsebody);
		    JSONArray arr = obj1.getJSONArray("transaction");
		    String brand = arr.getJSONObject(0).get("brand").toString();
		    JSONObject obj2 = new JSONObject(brand);
			token = obj2.getString("claimURL").substring(24);
			System.out.println("The token from confirm order is " + token);
}
	
	 @DataProviderParameter("FileName=" + Constants.testData_querytxn + ";Sheet=Sheet1")	
	 @Test(priority=3,dependsOnMethods = {"post_confirmorder"},dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
	 public void post_querytxn(String URI, String clientId, String apiKey, String OcpApimSubscriptionKey) throws JSONException 
	 {  
		 	RestAssured.baseURI = URI;
		    RequestSpecification request = RestAssured.given();
		    System.out.println("\nNow Doing Query Txn" );
		    request.header("Content-Type", "application/json").
		    header("clientId", clientId).
		    header("apiKey", apiKey).
		    header("Ocp-Apim-Subscription-Key", OcpApimSubscriptionKey);
		    
		    JSONObject requestParams = new JSONObject();			
		    requestParams.put("token", token);
		    
	    	request.body(requestParams.toString());	
			
			Response response = request.post(URI+"/api/v1/redemption/transaction");
					 
		    int statusCode = response.getStatusCode();
		    //Assert that correct status code is returned.
		    Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
		    
			String responsebody= response.getBody().asString();
			JSONObject json = new JSONObject(responsebody);
			System.out.println("The response from query txn is:\n" + json.toString(10));
				 
			JsonPath jsonPathEvaluator = response.jsonPath();
			int status= jsonPathEvaluator.get("statusCode");
			Assert.assertEquals(status, 200);
			
			voucherCode = jsonPathEvaluator.get("voucherCode");
			System.out.println("The voucherCode from query txn is " + voucherCode);	       
}
	
	 @DataProviderParameter("FileName=" + Constants.testData_validatetxn + ";Sheet=Sheet1")	
	 @Test(priority=4,dependsOnMethods = {"post_querytxn"},dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
	 public void post_validatetxn(String URI, String clientId, String apiKey, String OcpApimSubscriptionKey) 
	 {  
		 	RestAssured.baseURI = URI;
		    RequestSpecification request = RestAssured.given();
		    System.out.println("\nNow Doing Validate Txn" );
		    request.header("Content-Type", "application/json").
		    header("clientId", clientId).
		    header("apiKey", apiKey).
		    header("Ocp-Apim-Subscription-Key", OcpApimSubscriptionKey);
		    
		    JSONObject requestParams = new JSONObject();
		    requestParams.put("pin", accessCode);
		    requestParams.put("voucherCode", voucherCode);
		    
	    	request.body(requestParams.toString());	
			
			Response response = request.post(URI+"/api/v1/redemption/transaction/validation");
					 
		    int statusCode = response.getStatusCode();
		    //Assert that correct status code is returned.
		    Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
		    
			String responsebody= response.getBody().asString();
			JSONObject json = new JSONObject(responsebody);
			System.out.println("The response from validate txn is:\n" + json.toString(10));
				 
			JsonPath jsonPathEvaluator = response.jsonPath();
			int status= jsonPathEvaluator.get("statusCode");
			Assert.assertEquals(status, 200);
			
			txn_id = jsonPathEvaluator.get("transactionId");
			System.out.println("The transactionId from validate txn is " + txn_id);	
 }
	
	 @DataProviderParameter("FileName=" + Constants.testData_confirmorder + ";Sheet=Sheet1")	
	 @Test(priority=5,dependsOnMethods = {"post_validatetxn"},dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
	 public void get_giftcardslist(String URI, String clientId, String apiKey, String OcpApimSubscriptionKey, String accountId) 
	 {		 	
		 	System.out.println("\nNow Doing Get Giftcards List");
		    Response response = RestAssured.given().header("clientId", clientId).header("apiKey", apiKey).header("Ocp-Apim-Subscription-Key", OcpApimSubscriptionKey).
			queryParam("accountId", accountId).queryParam("transactionId", txn_id).when().get(URI + "/api/v1/redemption/giftcards/swychable");
		    
		    int statusCode = response.getStatusCode();
		    //Assert that correct status code is returned.
		    Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
		    
			String responsebody= response.getBody().asString();
			JSONObject json = new JSONObject(responsebody);
			System.out.println("The response from get giftcards list is:\n" + json.toString(10));
				 
			JsonPath jsonPathEvaluator = response.jsonPath();
			int status= jsonPathEvaluator.get("statusCode");
			Assert.assertEquals(status, 200);	
			
//			JSONArray catalog_arr = jsonPathEvaluator.get("catalog");			
	 }
	 
	 @DataProviderParameter("FileName=" + Constants.testData_swychgift + ";Sheet=Sheet1")	
	 @Test(priority=6,dependsOnMethods = {"get_giftcardslist"},dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
	 public void post_swychgift(String URI, String clientId, String apiKey, String OcpApimSubscriptionKey, String accountId, String intendedGiftcardId, String amount) 
	 {  
		 	RestAssured.baseURI = URI;
		    RequestSpecification request = RestAssured.given();
		    System.out.println("\nNow Doing Swych Gift" );
		    request.header("Content-Type", "application/json").
		    header("clientId", clientId).
		    header("apiKey", apiKey).
		    header("Ocp-Apim-Subscription-Key", OcpApimSubscriptionKey);
		    
		    JSONObject requestParams = new JSONObject();
		    requestParams.put("transactionId", txn_id);
		    requestParams.put("accountId", accountId);
		    requestParams.put("intendedGiftcardId", intendedGiftcardId);
		    requestParams.put("amount", amount);
		    
	    	request.body(requestParams.toString());	
			
			Response response = request.post(URI+"/api/v1/redemption/giftcards/swych");
					 
		    int statusCode = response.getStatusCode();
		    //Assert that correct status code is returned.
		    Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
		    
			String responsebody= response.getBody().asString();
			JSONObject json = new JSONObject(responsebody);
			System.out.println("The response from swych gift is:\n" + json.toString(10));
				 
			JsonPath jsonPathEvaluator = response.jsonPath();
			boolean success = jsonPathEvaluator.get("success");
			Assert.assertEquals(success, true);			
 }
}
