package Utilities;

import java.io.File;

public class Constants {
		
	public static final String CONFIG_PROPERTY_FILENAME = "src/main/java" + File.separator + "global.properties";	
	public static final String dataProviderClass1="dataProviderClass = gci.util.dataproviders.ExcelFileDataProvider.class";
	public static final String dataProvider = "getDataFromExcelFile";
	public static final String dataProviderClass = "gci.util.dataproviders.ExcelFileDataProvider.class";
	public static final String testData_coinpayments_prod = "/CoinPayments/coinpayments_prod.xls";
	public static final String testData_coinpayments_qa = "/CoinPayments/coinpayments_qa.xls";
	public static final String testData_coinpayments_preprod = "/CoinPayments/coinpayments_preprod.xls";
	public static final String testData_srp_qa_2 = "/SRP/srp_qa_2.xls";
	public static final String testData_srp_qa_3 = "/SRP/srp_qa_3.xls";
	public static final String testData_srp_preprod_2 = "/SRP/srp_preprod_2.xls";
	public static final String testData_srp_preprod_3 = "/SRP/srp_preprod_3.xls";
	public static final String testData_srp_prod_2 = "/SRP/srp_prod_2.xls";
	public static final String testData_srp_prod_3 = "/SRP/srp_prod_3.xls";






}
