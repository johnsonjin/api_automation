package CoinPayments;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import Utilities.Constants;
import Utilities.DataProviderParameter;
import Utilities.TestBase;
import io.restassured.path.json.JsonPath;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class CoinPayments_prod extends TestBase {
	
	 String orderId =""; 
	 String token = "";
	 String voucherCode = "";
	 String txn_id = "";
	 String accessCode = "";
	 String access_token = "";
	//test
	 @DataProviderParameter("FileName=" + Constants.testData_coinpayments_prod + ";Sheet=createorder")	
	 @Test(priority = 3, dataProviderClass = Utilities.ExcelFileDataProvider.class, dataProvider = "getDataFromExcelFile")
	 public void put_createorder(String URI, String clientId, String apiKey, String OcpApimSubscriptionKey,
			String accountId, String brandId, String productId, String amount, String senderFirstName ,String senderLastName, String senderEmail, String senderPhone, String recipientFirstName, 
			String recipientLastName, String recipientEmail, String recipientPhoneNumber, String notificationDeliveryMethod, String giftDeliveryMode, String swychable, String deliverGiftTo) throws JSONException 
	 {  
		 	RestAssured.baseURI = URI;
		    RequestSpecification request = RestAssured.given();
		    System.out.println("\nNow Creating Order " );
		    request.header("Content-Type", "application/json").
		    header("Authorization", access_token).
		    header("clientId", clientId).
		    header("apiKey", apiKey).
		    header("Ocp-Apim-Subscription-Key", OcpApimSubscriptionKey);
		    //Test
		    JSONObject requestParams = new JSONObject();		    
		    requestParams.put("accountId", accountId);
		    requestParams.put("brandId", brandId);
	    	requestParams.put("productId", productId);
	    	requestParams.put("amount", amount);
	    	requestParams.put("senderFirstName", senderFirstName);
			requestParams.put("senderLastName", senderLastName);
	    	requestParams.put("recipientFirstName", recipientFirstName);
			requestParams.put("recipientLastName", recipientLastName);
		    requestParams.put("senderEmail", senderEmail);
		    requestParams.put("senderPhone", senderPhone);
	    	requestParams.put("recipientEmail", recipientEmail);
	    	requestParams.put("recipientPhoneNumber", recipientPhoneNumber);
	    	requestParams.put("notificationDeliveryMethod", notificationDeliveryMethod);
		    requestParams.put("giftDeliveryMode", giftDeliveryMode);
	    	requestParams.put("swychable", swychable);
	    	requestParams.put("deliverGiftTo", deliverGiftTo);	    	
	    
	    	request.body(requestParams.toString());
			 
			Response response = request.put(URI + "/api/v1/b2b/order");
					 
		    int statusCode = response.getStatusCode();
		    //Assert that correct status code is returned.
		    Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
		    
			String responsebody = response.getBody().asString();
			JSONObject json = new JSONObject(responsebody);
			System.out.println("The response from create order is:\n" + json.toString(10));
				 
			JsonPath jsonPathEvaluator = response.jsonPath();
			int status = jsonPathEvaluator.get("statusCode");
			Assert.assertEquals(status, 16010900);
			
			orderId = jsonPathEvaluator.get("orderId");
			System.out.println("The order id is " + orderId);	 
			
			accessCode = jsonPathEvaluator.get("AccessCode");
			System.out.println("The accessCode is " + accessCode);	 
 }
	
	 @DataProviderParameter("FileName=" + Constants.testData_coinpayments_prod + ";Sheet=confirmorder")	
	 @Test(priority = 4, dataProviderClass = Utilities.ExcelFileDataProvider.class, dataProvider = "getDataFromExcelFile")
	 public void post_confirmorder(String URI, String clientId, String apiKey, String OcpApimSubscriptionKey, String accountId) throws JSONException 
	 {		 
		 	RestAssured.baseURI = URI;
		    RequestSpecification request = RestAssured.given();
		    System.out.println("\nNow Confirming Order " );
		    request.header("Content-Type", "application/json").
		    header("Authorization", access_token).
		    header("clientId", clientId).
		    header("apiKey", apiKey).
		    header("Ocp-Apim-Subscription-Key", OcpApimSubscriptionKey);
		    
		    JSONObject requestParams = new JSONObject();		    
		    requestParams.put("accountId", accountId);
		    requestParams.put("orderId", orderId);
		    requestParams.put("accessCode", accessCode);
	    				
	    	request.body(requestParams.toString());			 
			
			Response response = request.post(URI + "/api/v1/b2b/order");
					 
		    int statusCode = response.getStatusCode();
		    //Assert that correct status code is returned.
		    Assert.assertEquals(statusCode /*actual value*/, 200 /*expected value*/, "Correct status code returned");
		    
		    String responsebody = response.getBody().asString();
			JSONObject json = new JSONObject(responsebody);
			System.out.println("The response from confirm order is:\n" + json.toString(10));
				 
			JsonPath jsonPathEvaluator = response.jsonPath();
			int status= jsonPathEvaluator.get("statusCode");
			Assert.assertEquals(status, 16011000);
			
			
		    JSONObject obj1 = new JSONObject(responsebody);
		    JSONArray arr = obj1.getJSONArray("transaction");
		    String brand = arr.getJSONObject(0).get("brand").toString();
		    JSONObject obj2 = new JSONObject(brand);
			token = obj2.getString("claimURL").substring(39);
			System.out.println("The token from confirm order is " + token);
}
	
	 @DataProviderParameter("FileName=" + Constants.testData_coinpayments_prod + ";Sheet=getbalance")
		@Test(priority=2,dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")

		 public void get_balance_before (String URI, String client_id, String APIKey, String OcpApimSubscriptionKey, String accountid) 
		 {  
			//String accountid="9185384047158163";
			System.out.println("\nNow Getting Balance " );
			 System.out.println("access token used in get balance is " +access_token);
			Response response_get_balance=RestAssured.given().header("Authorization", access_token).
					header("clientId",client_id).
					header("ApiKey",APIKey).
					header("Ocp-Apim-Subscription-Key",OcpApimSubscriptionKey).
					get(URI+"/api/v1/b2b/"+accountid+"/balance");
			
			int statusCode_get_balance = response_get_balance.getStatusCode();
			 
			 String responsebody_get_balance= response_get_balance.getBody().asString();
			 System.out.println("The response from get balance is "+responsebody_get_balance);
			 System.out.println("The statuscode is "+statusCode_get_balance);
			 
			 JsonPath jsonPathEvaluator_get_balance = response_get_balance.jsonPath();
			 int status= jsonPathEvaluator_get_balance.get("statusCode");
			 //int status = Integer.parseInt(sstatus);
			 Assert.assertEquals(status, 16010600);
			
		 }
	 
	 @DataProviderParameter("FileName=" + Constants.testData_coinpayments_prod + ";Sheet=getbalance")
		@Test(priority=5,dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")

		 public void get_balance_after (String URI, String client_id, String APIKey, String OcpApimSubscriptionKey,String accountid ) 
		 {  
			//String accountid="9185384047158163";
			System.out.println("\nNow Getting Balance " );
			 System.out.println("access token used in get balance is " +access_token);
			Response response_get_balance=RestAssured.given().header("Authorization", access_token).
					header("clientId",client_id).
					header("ApiKey",APIKey).
					header("Ocp-Apim-Subscription-Key",OcpApimSubscriptionKey).
					get(URI+"/api/v1/b2b/"+accountid+"/balance");
			
			int statusCode_get_balance = response_get_balance.getStatusCode();
			 
			 String responsebody_get_balance= response_get_balance.getBody().asString();
			 System.out.println("The response from get balance is "+responsebody_get_balance);
			 System.out.println("The statuscode is "+statusCode_get_balance);
			 
			 JsonPath jsonPathEvaluator_get_balance = response_get_balance.jsonPath();
			 int status= jsonPathEvaluator_get_balance.get("statusCode");
			 //int status = Integer.parseInt(sstatus);
			 Assert.assertEquals(status, 16010600);
			
		 }
	 	@DataProviderParameter("FileName=" + Constants.testData_coinpayments_prod + ";Sheet=getcatalog")		
		@Test(priority=1,dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")

		 public void get_catalog(String URI, String client_id,String APIKey, String OcpApimSubscriptionKey,String accountid ) 
		 {  
			//String accountid="9185384047158163";
			 System.out.println("\nNow Getting Catalog " );
			 System.out.println("access token used in get catalog is " +access_token);
			Response response_get_balance=RestAssured.given().header("Authorization", access_token).
					header("clientId",client_id).
					header("ApiKey",APIKey).
					header("Ocp-Apim-Subscription-Key",OcpApimSubscriptionKey).
					get(URI+"/api/v1/b2b/"+accountid+"/catalog");
			
			int statusCode_get_balance = response_get_balance.getStatusCode();
			 
			 String responsebody_get_balance= response_get_balance.getBody().asString();
			 System.out.println("The response from get catalog is "+responsebody_get_balance);
			 System.out.println("The statuscode is "+statusCode_get_balance);
			 
			 JsonPath jsonPathEvaluator_get_balance = response_get_balance.jsonPath();
			 int status= jsonPathEvaluator_get_balance.get("statusCode");
			 //int status = Integer.parseInt(sstatus);
			 Assert.assertEquals(status, 16010800);
			
				 
		 
	 }	 
	 	@DataProviderParameter("FileName=" + Constants.testData_coinpayments_prod + ";Sheet=suggestedrate")		
		@Test(priority=7,dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
		 public void post_suggestedrate(String URI, String client_id, String APIKey,
					String OcpApimSubscriptionKey,String sourceCurrency,String targetCurrency) throws JSONException 
		 {  
			 
			 RestAssured.baseURI = URI;
			    RequestSpecification request = RestAssured.given();
			    System.out.println("\nNow Doing Suggested Rate " );
			    request.header("Content-Type", "application/json").
			    header("Authorization",access_token).
			    header("clientId",client_id).
			    header("ApiKey",APIKey).
			    header("Ocp-Apim-Subscription-Key",OcpApimSubscriptionKey);
			    
			    JSONObject requestParams = new JSONObject();
			    
			    requestParams.put("sourceCurrency", sourceCurrency);
			    requestParams.put("targetCurrency", targetCurrency);
		    	
		    	
				
		    	request.body(requestParams.toString());
				 
				
				 Response response = request.post(URI+"/api/v1/b2b/suggestedRate");
						 

				    int statusCode = response.getStatusCode();
					 String responsebody= response.getBody().asString();
					 System.out.println("The response from create Order is "+responsebody);
					 /*
					 JsonPath jsonPathEvaluator = response.jsonPath();
					 int status= jsonPathEvaluator.get("statusCode");
					 //int status = Integer.parseInt(sstatus);
					 Assert.assertEquals(status, 16011000);
					 */
		       
	}
	 	
	 	@DataProviderParameter("FileName=" + Constants.testData_coinpayments_prod + ";Sheet=gethistory")
	 	@Test(priority=6,dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")
	 	
		 public void get_history (String URI, String client_id,String APIKey, 
					String OcpApimSubscriptionKey,String accountid, String startDate,String endDate,String pageNo,String pageSize) throws JSONException 
		 {  
			 System.out.println("\nNow Getting History " );
			 System.out.println("access token used in get catalog is " +access_token);
			 Response response_get_history = RestAssured.given().header("Authorization", access_token).
					header("clientId",client_id).
					header("ApiKey",APIKey).
				    header("Ocp-Apim-Subscription-Key",OcpApimSubscriptionKey).
					queryParam("startDate",startDate).
					queryParam("endDate",endDate).
					queryParam("pageNo",1).
					queryParam("pageSize",5).when().get(URI+"/api/v1/b2b/"+accountid+"/history");
			
			 int statusCode_get_history = response_get_history.getStatusCode();
			 
			 String responsebody_get_history= response_get_history.getBody().asString();
			 System.out.println("The response from get history is "+responsebody_get_history);
			 System.out.println("The statuscode is "+statusCode_get_history);
			 
			 JsonPath jsonPathEvaluator_get_balance = response_get_history.jsonPath();
			 int status= jsonPathEvaluator_get_balance.get("statusCode");
			 Assert.assertEquals(status, 16011100);
	 }
	 	
	 	@DataProviderParameter("FileName=" + Constants.testData_coinpayments_prod + ";Sheet=getorderdetails")		
		@Test(priority=8,dataProviderClass = Utilities.ExcelFileDataProvider.class,dataProvider = "getDataFromExcelFile")

		 public void get_order_details(String URI, String client_id, String APIKey, String OcpApimSubscriptionKey,String accountid) 
		 {  
			
			 System.out.println("\nNow Order Details " );
			 System.out.println("access token used in get order details is " +access_token);
			 Response response_get_order_details=RestAssured.given().header("Authorization", access_token).header("clientId",client_id).header("ApiKey",APIKey).header("Ocp-Apim-Subscription-Key",OcpApimSubscriptionKey).get(URI+"/api/v1/b2b/"+accountid+"/order?orderId="+orderId);
			 int statusCode_get_order_details = response_get_order_details.getStatusCode();
			 String responsebody_get_order_details= response_get_order_details.getBody().asString();
			 System.out.println("The response from get order_details is "+responsebody_get_order_details);
			 System.out.println("The statuscode is "+statusCode_get_order_details); 
			 JsonPath jsonPathEvaluator_get_order_details = response_get_order_details.jsonPath();
			 int status= jsonPathEvaluator_get_order_details.get("statusCode");
			 Assert.assertEquals(status, 16011200);						 
		 
	 }	
		
	 @DataProviderParameter("FileName=" + Constants.testData_coinpayments_prod + ";Sheet=authentication")	
	 @Test(priority = 0, dataProviderClass = Utilities.ExcelFileDataProvider.class, dataProvider = "getDataFromExcelFile")
	 public void POST_authentication (String URI, String ContentType, String OcpApimSubscriptionKey, String grant_type, 
			 String client_id, String client_secret, String resource) throws JSONException
     {   
		 	RestAssured.baseURI = URI;
		 	Response response = RestAssured.given().header("Content-Type", ContentType).
			header("Ocp-Apim-Subscription-Key", OcpApimSubscriptionKey).
			formParam("grant_type", grant_type).
			formParam("client_id", client_id).
			formParam("client_secret", client_secret).request().
			formParam("resource", resource).
			post(URI + "/oauth2/token");
	 	
		 	int statusCode = response.getStatusCode();
	 
		 	String responsebody= response.getBody().asString();
		 	System.out.println("The response from POST Authetication is " + responsebody);
		 	System.out.println("The statuscode is " + statusCode);
	 
		 	JsonPath jsonPathEvaluator = response.jsonPath();
		 	access_token = jsonPathEvaluator.get("access_token");
		 	System.out.println("access token Response " + access_token);
}
}

