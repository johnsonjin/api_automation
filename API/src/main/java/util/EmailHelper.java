package util;

import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/*
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;


//1.Login to Gmail.
//2.Access the URL as https://www.google.com/settings/security/lesssecureapps
//3.Select "Turn on"

//Enabled the IMAP access from gmail setting (Forwarding and POP/IMAP) 

public class EmailHelper {

	TestBase base = new TestBase();

	public String getLinkFromEmail(String userName, String password, String linkText) throws Exception {
		String URL = null;
		Folder folder = null;
		Store store = null;
		System.out.println("***READING MAILBOX...");
		try {
			Properties props = new Properties();
			props.put("mail.store.protocol", "imaps");

			Session session = Session.getInstance(props);
			store = session.getStore("imaps");
			store.connect("imap.gmail.com", userName, password);
			folder = store.getFolder("INBOX");
			folder.open(Folder.READ_WRITE);

			Message[] messages = folder.getMessages();
			int len = folder.getMessageCount();
			for (int i = len - 1; 0 <= len - 1; len--) {
				String emailBody = getMessageHtmlBody(messages[i]);
				if (emailBody.contains(linkText)) {
					URL = getUrlsFromMessage(messages[i], linkText);
					break;
				}

			}

		} catch (MessagingException messagingException) {
			messagingException.printStackTrace();
		} finally {
			if (folder != null) {
				try {
					folder.close(true);
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (store != null) {
				try {
					store.close();
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return URL;
	}

	public String getUrlsFromMessage(Message message, String linkText) throws Exception {
		String html = getMessageHtmlBody(message);
		String URL = null;
		String actualURL = null;

		Matcher matcher = Pattern.compile("(<a [^>]+>)" + linkText + "</a>").matcher(html);
		while (matcher.find()) {
			URL = matcher.group(1);
			String url[] = URL.toString().split("'");
			String[] actualURL1 = url[1].split(">");
			actualURL = actualURL1[0];
			break;
		}
		return actualURL;
	}

	private String getMessageHtmlBody(Message message) throws Exception {
		String result = null;
		if (message.isMimeType("text/plain")) {
			return message.getContent().toString();
		} else if (message.isMimeType("multipart/*")) {
			result = "";
			MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
			int count = mimeMultipart.getCount();
			for (int i = 0; i < count; i++) {

				BodyPart bodyPart = mimeMultipart.getBodyPart(i);
				String html = (String) bodyPart.getContent();
				result = html;
			}
			return result;
		}
		return result;
	}
}
*/
