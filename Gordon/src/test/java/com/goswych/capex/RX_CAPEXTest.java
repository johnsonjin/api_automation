package com.goswych.capex;

import com.google.gson.Gson;
import com.goswych.capex.request.RX_RequestCreateOrder;
import com.goswych.capex.request.RX_RequestSuggestedRate;
import com.goswych.capex.response.RX_ResponseBalance;
import com.goswych.capex.response.RX_ResponseCategory;
import com.goswych.capex.response.RX_ResponseCreateOrder;
import com.goswych.capex.response.RX_ResponseOAuth2;
import com.goswych.capex.response.RX_ResponseSuggestedRate;
import com.goswych.capex.service.RX_CAPEXservice;
import com.goswych.net.PullRXObserver;
import com.goswych.net.RestServiceFactory;

//import io.restassured.RestAssured;
//import io.restassured.path.json.JsonPath;
//import io.restassured.response.Response;
//import io.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.Map;
import retrofit2.HttpException;

//import org.json.JSONException;
//import org.json.JSONObject;
//import org.testng.Assert;

//import io.reactivex.Observable;
//import io.restassured.RestAssured;
//import io.restassured.http.Header;
//import io.restassured.path.json.JsonPath;
//import io.restassured.response.Response;
//import io.restassured.specification.RequestSpecification;
//
//import org.json.JSONObject;
//import org.json.JSONException;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//import com.sun.xml.xsom.impl.Ref.ContentType;
//import io.restassured.http.Header;
//import io.restassured.http.Method;
//import io.restassured.path.json.JsonPath;
//import io.restassured.RestAssured;
//import io.restassured.response.Response;
//import io.restassured.response.ResponseBody;
//import io.restassured.specification.RequestSpecification;

public class RX_CAPEXTest {
    static String URI = "https://api.swychover.io/sandbox";

    static String access_token = "";
    static String clinet_id = "swych_b2b_capex";
    static String accountid = "9185384047158163";
    static String ApiKey = "d2ae25t7197f11e793ae9r2e63269v5t";
    static String orderId = "";

    
    Map<String, String> dynamicHeaders = null;
    
    static RX_CAPEXservice restService = null;

    static {
        try {
            restService = RestServiceFactory.createService(RX_CAPEXservice.class, URI, true);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    
	@DataProvider(name = "Authentication")
	@BeforeTest
	public void postAuthen() {
        assert restService  != null;

		//Response observed
        PullRXObserver<RX_ResponseOAuth2> observer = new PullRXObserver<>("synObject");

        //do request
        restService.postOauth2Token(
        		"client_credentials", 
        		"56a28836-6743-4249-b22b-5e1a224b1693",
        		"Y[52y.pyQXc+BLfhwy/2jkcqYXq00w5L",
        		"3a8271ef-ef89-445c-bca2-a0a66d1afc52"
        		)
        .subscribe(observer);
		
        //possible exceptions
        Throwable ex = observer.getThrowable();
        HttpException httpEx = observer.getHttpEx();
        System.out.println("observer.getThrowable() " + ex);
        System.out.println("observer.getHttpEx() " + httpEx);
        if(ex != null && httpEx == null){
            System.out.println("network error");
        }
        if(httpEx != null){
            System.out.println("http error");
            System.out.println("http status code = " + httpEx.code());
        }
        
        assert observer.getThrowable()  == null;	//no network err
        assert observer.getHttpEx() == null; 		//http statusCode == HttpURLConnection.HTTP_OK == 200

        RX_ResponseOAuth2 response =  observer.getResp();

        assert response != null; //should have response

        access_token = response.access_token;
		
//		// Specify the base URL to the RESTful web service
//		RestAssured.baseURI = URI;
//		
//		Header header = new Header("Content-Type", "application/x-www-form-urlencoded");
//		Response response = RestAssured.given()
//				.header("Content-Type", "application/x-www-form-urlencoded")
//				.header("Ocp-Apim-Subscription-Key", "77ad354426ab4a39a16923417829661b")
//				
//				
//				.formParam("grant_type", "client_credentials")
//				.formParam("client_id", "56a28836-6743-4249-b22b-5e1a224b1693")
//				.formParam("client_secret", "Y[52y.pyQXc+BLfhwy/2jkcqYXq00w5L")
//				.request().formParam("resource", "3a8271ef-ef89-445c-bca2-a0a66d1afc52")
//				.post(URI + "/oauth2/token");
//
//		int statusCode = response.getStatusCode();
//
//		String responsebody = response.getBody().asString();
//		System.out.println("The response from POST Authetication is " + responsebody);
//		System.out.println("The statuscode is " + statusCode);
//
//		JsonPath jsonPathEvaluator = response.jsonPath();
//		access_token = jsonPathEvaluator.get("access_token");
		
		System.out.println("access token Response " + access_token);
		System.out.println("Done");
		
		// String token = response.body().jsonPath().get("token");
		// System.out.println("The responsebody is "+responsebody);

		// String access_token= responsebody.substring(185,1352);
		// int length = access_token.length();
		// System.out.println("Lenght is"+length);
		// System.out.println("The responsebody is "+access_token);
		// return token;

		// Accountid = 9185384047158163
		// Clinet id = swych_b2b_capex
		// API Key = d2ae25t7197f11e793ae9r2e63269v5t

	}

	private Map<String, String> getHeaders(){
		if(dynamicHeaders == null) {
	        dynamicHeaders = new HashMap<>();
	        dynamicHeaders.put("Authorization", access_token);
	        dynamicHeaders.put("clientId", clinet_id);
	        dynamicHeaders.put("ApiKey", "d2ae25t7197f11e793ae9r2e63269v5t");
	        dynamicHeaders.put("ocp-apim-subscription-key", "partha@goswych.com");
		}
		return dynamicHeaders;
	
	}
	
	@Test(priority=1)
	public void putCreateOrder(){

		System.out.println("\nNow Creating Order ");

		RX_RequestCreateOrder order = new RX_RequestCreateOrder(); 
		order.accountId = accountid;
		order.brandId = "7";
		order.productId = "7-0";
		order.amount = "10";

		order.senderFirstName = "Partha";
		order.senderLastName = "Pattanaik";
		order.senderemail = "partha@goswych.com";
		order.senderPhone = "4156969775";

		order.recipientemail = "partha@goswych.com";
		order.recipientPhoneNumber = "4156969775";
		order.notificationDeliveryMethod = 0;
		order.giftDeliveryMode = "1";
		order.swychable = true;
		
        //Response observed
        PullRXObserver<RX_ResponseCreateOrder> observer = new PullRXObserver<RX_ResponseCreateOrder>("synObject");
        //do request
        restService.putCreateOrder(getHeaders(), order).subscribe(observer);

        assert observer.getThrowable()  == null;	//no network err
        assert observer.getHttpEx() == null; 		//http statusCode == HttpURLConnection.HTTP_OK == 200

        RX_ResponseCreateOrder response =  observer.getResp();

        assert response != null; //should have response

        System.out.println("RX_ResponseCreateOrder response =  " + response);

        assert response.statusCode == 16010900;
        
        orderId = response.orderId;
        
        System.out.println("The order id is " + orderId);
		
//		RestAssured.baseURI = URI;
//		RequestSpecification request = RestAssured.given();
//		
//		request
//		.header("Content-Type", "application/json")
//		.header("Authorization", access_token)
//		.header("clientId", clinet_id)
//		.header("ApiKey", ApiKey)
//		.header("ocp-apim-subscription-key", "partha@goswych.com");
//      
//		JSONObject requestParams = new JSONObject();
//		requestParams.put("accountId", accountid);
//		requestParams.put("brandId", "7");
//		requestParams.put("productId", "7-0");
//		requestParams.put("amount", "10");
//
//		requestParams.put("senderFirstName", "Partha");
//		requestParams.put("senderLastName", "Pattanaik");
//		requestParams.put("senderemail", "partha@goswych.com");
//		requestParams.put("senderPhone", "4156969775");
//
//		requestParams.put("recipientemail", "partha@goswych.com");
//		requestParams.put("recipientPhoneNumber", "4156969775");
//		requestParams.put("notificationDeliveryMethod", 0);
//		requestParams.put("giftDeliveryMode", "1");
//		requestParams.put("swychable", "True");
//
//		request.body(requestParams.toString());
//
//		Response response = request.put(URI + "/api/v1/b2b/order");
//
//		int statusCode = response.getStatusCode();
//		String responsebody = response.getBody().asString();
//		System.out.println("The response from create Order is " + responsebody);
//
//		JsonPath jsonPathEvaluator = response.jsonPath();
//		int status = jsonPathEvaluator.get("statusCode");
		// int status = Integer.parseInt(sstatus);
//		
//		
//		Assert.assertEquals(status, 16010900);
//		orderId = jsonPathEvaluator.get("orderId");
//		System.out.println("The order id is " + orderId);

	}
	
    
    @Test(priority=2)
    public void getBalance() {
    	
        assert restService  != null;

        System.out.println("\nNow Getting Balance ");
        System.out.println("access token used in get balance is " + access_token);

        //Response observed
        PullRXObserver<RX_ResponseBalance> observer = new PullRXObserver<>("synObject");

        //do request
        restService.getBalance(getHeaders(), accountid).subscribe(observer);
        //restService.getBalanceWithAccId(dynamicHeaders).subscribe(observer);

        assert observer.getThrowable()  == null;	//no network err
        assert observer.getHttpEx() == null; 		//http statusCode == HttpURLConnection.HTTP_OK == 200
        RX_ResponseBalance response =  observer.getResp();
        assert response != null; //should have response
        System.out.println("RX_ResponseBalance response =  " + response);
        assert response.statusCode == 16010600;
        
//		Response response_get_balance = RestAssured.given()
//				.header("Authorization", access_token)
//				.header("clientId", clinet_id)
//				.header("ApiKey", "d2ae25t7197f11e793ae9r2e63269v5t")
//				.header("ocp-apim-subscription-key", "partha@goswych.com")
//				
//				.get(URI + "/api/v1/b2b/" + accountid + "/catalog");
//
//		int statusCode_get_balance = response_get_balance.getStatusCode();
//
//		String responsebody_get_balance = response_get_balance.getBody().asString();
//		System.out.println("The response from get catalog is " + responsebody_get_balance);
//		System.out.println("The statuscode is " + statusCode_get_balance);
//
//		JsonPath jsonPathEvaluator_get_balance = response_get_balance.jsonPath();
//		int status = jsonPathEvaluator_get_balance.get("statusCode");
//		// int status = Integer.parseInt(sstatus);
//		Assert.assertEquals(status, 16010800);
        
    }
    
	@Test (priority=3)
	public void getCatalog() {
		// String accountid="9185384047158163";
		System.out.println("\nNow Getting Catalog ");
		System.out.println("access token used in get catalog is " + access_token);
		
        //Response observed
        PullRXObserver<RX_ResponseCategory> observer = new PullRXObserver<>("synObject");

        //do request
        restService.geCatalog(getHeaders(), accountid).subscribe(observer);
        //restService.getBalanceWithAccId(dynamicHeaders).subscribe(observer);

        assert observer.getThrowable()  == null;	//no network err
        assert observer.getHttpEx() == null; 		//http statusCode == HttpURLConnection.HTTP_OK == 200
        RX_ResponseCategory response =  observer.getResp();
        assert response != null; //should have response
        System.out.println("RX_ResponseCategory response =  " + new Gson().toJson(response) );
        assert response.statusCode == 16010800;
        
//		
//		Response response_get_balance = RestAssured.given()
//				.header("Authorization", access_token)
//				.header("clientId", clinet_id)
//				.header("ApiKey", "d2ae25t7197f11e793ae9r2e63269v5t")
//				.header("ocp-apim-subscription-key", "partha@goswych.com")
//				
//				.get(URI + "/api/v1/b2b/" + accountid + "/catalog");
//
//		int statusCode_get_balance = response_get_balance.getStatusCode();
//
//		String responsebody_get_balance = response_get_balance.getBody().asString();
//		System.out.println("The response from get catalog is " + responsebody_get_balance);
//		System.out.println("The statuscode is " + statusCode_get_balance);
//
//		JsonPath jsonPathEvaluator_get_balance = response_get_balance.jsonPath();
//		int status = jsonPathEvaluator_get_balance.get("statusCode");
//		// int status = Integer.parseInt(sstatus);
//		Assert.assertEquals(status, 16010800);
		/*
		 * if (status=16010600) { System.out.println("The test is a success"); }else {
		 * System.out.println("The test is a fail"); }
		 */

	}
	
	@Test (priority=4)
	public void postSuggestedRate(){

		System.out.println("\nNow Doing Suggested Rate ");
		
        //Response observed
        PullRXObserver<RX_ResponseSuggestedRate> observer = new PullRXObserver<>("synObject");

        RX_RequestSuggestedRate req = new RX_RequestSuggestedRate();
        req.sourceCurrency = "INR";
        req.targetCurrency = "USD";
        		
        //do request
        restService.postSuggestedRate(getHeaders(), req).subscribe(observer);

        assert observer.getThrowable()  == null;	//no network err
        assert observer.getHttpEx() == null; 		//http statusCode == HttpURLConnection.HTTP_OK == 200
        RX_ResponseSuggestedRate response =  observer.getResp();
        assert response != null; //should have response
        System.out.println("RX_RequestSuggestedRate response =  " + new Gson().toJson(response) );
        assert response.statusCode == 16011300;

//		RestAssured.baseURI = URI;
//		RequestSpecification request = RestAssured.given();
//		request
//		.header("Content-Type", "application/json")
//		
//		.header("Authorization", access_token)
//		.header("clientId", clinet_id)
//		.header("ApiKey", ApiKey)
//		.header("ocp-apim-subscription-key", "partha@goswych.com");
//
//		JSONObject requestParams = new JSONObject();
//
//		requestParams.put("sourceCurrency", "INR");
//		requestParams.put("targetCurrency", "USD");
//
//		request.body(requestParams.toString());
//
//		Response response = request.post(URI + "/api/v1/b2b/suggestedRate");
//
//		int statusCode = response.getStatusCode();
//		String responsebody = response.getBody().asString();
//		System.out.println("The response from create Order is " + responsebody);
//		/*
//		 * JsonPath jsonPathEvaluator = response.jsonPath(); int status=
//		 * jsonPathEvaluator.get("statusCode"); //int status =
//		 * Integer.parseInt(sstatus); Assert.assertEquals(status, 16011000);
//		 */

	}
	

//	/**
//	 * not converted as some repose data type is unknown.
//	 */
//	@Test(dependsOnMethods = { "putCreateOrder" })
//	public void postConfirmOrder() {
//
//		System.out.println("\nNow Confirming Order ");
//		
//		RestAssured.baseURI = URI;
//		RequestSpecification request = RestAssured.given();
//		request.header("Content-Type", "application/json")
//		.header("Authorization", access_token)
//		.header("clientId", clinet_id)
//		.header("ApiKey", ApiKey)
//		.header("ocp-apim-subscription-key", "partha@goswych.com");
//
//		JSONObject requestParams = new JSONObject();
//
//		requestParams.put("accountId", accountid);
//		requestParams.put("orderId", orderId);
//
//		request.body(requestParams.toString());
//
//		Response response = request.post(URI + "/api/v1/b2b/order");
//
//		int statusCode = response.getStatusCode();
//		String responsebody = response.getBody().asString();
//		System.out.println("The response from create Order is " + responsebody);
//
//		JsonPath jsonPathEvaluator = response.jsonPath();
//		int status = jsonPathEvaluator.get("statusCode");
//		// int status = Integer.parseInt(sstatus);
//		Assert.assertEquals(status, 16011000);
//
//	}
}
