package com.goswych.capex.service;

import java.util.Map;

import com.goswych.capex.request.RX_RequestCreateOrder;
import com.goswych.capex.request.RX_RequestSuggestedRate;
import com.goswych.capex.response.RX_ResponseBalance;
import com.goswych.capex.response.RX_ResponseCategory;
import com.goswych.capex.response.RX_ResponseCreateOrder;
import com.goswych.capex.response.RX_ResponseOAuth2;
import com.goswych.capex.response.RX_ResponseSuggestedRate;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RX_CAPEXservice {


    @Headers({
        "Content-Type: application/x-www-form-urlencoded",
        "Ocp-Apim-Subscription-Key: 77ad354426ab4a39a16923417829661b",
        "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:70.0) Gecko/20100101 Firefox/70.0",
        
	})
    @FormUrlEncoded    
	@POST("oauth2/token")
	Observable<RX_ResponseOAuth2> postOauth2Token(
			@Field("grant_type") String grant_type,
			@Field("client_id") String client_id,
			@Field("client_secret") String client_secret,
			@Field("resource") String resource
			);
	
	
    @Headers({
        "Accept: application/json",
        "Content-Type: application/json",
	})
	@PUT("api/v1/b2b/order")
	Observable<RX_ResponseCreateOrder> putCreateOrder(@HeaderMap Map<String, String> headers,
            @Body RX_RequestCreateOrder json);
			
	
	@Headers({
        "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("api/v1/b2b/{accountid}/balance")
    Observable<RX_ResponseBalance> getBalance(@HeaderMap Map<String, String> headers,
                                  @Path("accountid") String accountid);

	@Headers({
        "Content-Type: application/x-www-form-urlencoded",
    })
    @GET("api/v1/b2b/{accountid}/catalog")
    Observable<RX_ResponseCategory> geCatalog(@HeaderMap Map<String, String> headers,
                                  @Path("accountid") String accountid);

    
    
    @Headers({
        "Accept: application/json",
        "Content-Type: application/json",
	})
	@POST("api/v1/b2b/suggestedRate")
	Observable<RX_ResponseSuggestedRate> postSuggestedRate(@HeaderMap Map<String, String> headers,
            @Body RX_RequestSuggestedRate json);
    
}
