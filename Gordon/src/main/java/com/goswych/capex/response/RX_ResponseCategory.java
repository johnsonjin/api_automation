package com.goswych.capex.response;

public class RX_ResponseCategory extends RX_ResponseBase {
    public String[]  categoryList;
    public Catalog[] catalog;
    
    public static class Catalog{
    	public Product [] products;
    	public int[] categories;
    	
    	public String brandId;//": "1",
    	public String brandName;//": "Amazon",
    	public String brandTC;//": "https://api.appcaps.com/swychapp2/gift_card_images/Amazon.html",
    	public String brandDisclaimer;//": null,
    	public String brandRedemptionInstruction;//": "https://api.appcaps.com/swychapp_dpl/gift_card_images/redemption_url_1.html",
    	public String brandDescription;//": "",
    	
    	public String brandImage;//": "https://api.appcaps.com/swych-content/gift_card_images/giftcard_default_1_1.png",
    	public int denominationType;//: 1,
    	public int denominationMin;//: 10.0,
    	public int denominationMax;//: 300.0,
    	public boolean canSendToFriend;//: true,
    	public boolean canSelfGifting;//: true,
    	public boolean canSwych;//: true,
    	public boolean canRedeem;//: true,
    	public boolean canRegift;//: false,
    	public boolean canCashout;//: false,
    	
    	public String countryCode;//": "USA",
    	public String currency;//": "USD",
    	public String currencySymbol;//": "$",
    	public String claimURL;//": "https://www.amazon.com/gc/redeem?claimCode=={0}",
    	public int availability;//": 1
		
    }
    
    public static class Product {
    	public String productId;//": "1-0",
    	public String productName;//": "ranged-product",
    	public int productType;//": 1,
    	public int amount;//": 0,
    	public int minimumAmount;//": 10,
    	public int maximumAmount;//": 300
    }
    

}
