package com.goswych.capex.response;

public class RX_ResponseBalance extends RX_ResponseBase {
    public float balance;// = 2035.0f;

    @Override
    public String toString(){
        return String.format(
                "{\"balance\":%f,\"success\":%b,\"message\":\"%s\",\"statusCode\":%d}",
                balance,success, message, statusCode
                );
    }
}
