package com.goswych.capex.response;

public class RX_ResponseSuggestedRate extends RX_ResponseBase {
	//{"suggestedRate":0.013886,"sourceCurrency":"INR","targetCurrency":"USD","success":true,"message":"Successfully get exchange rate.","statusCode":16011300}
	public float suggestedRate;//":0.013886,
	public String sourceCurrency;//":"INR",
	public String targetCurrency;//":"USD",
}
