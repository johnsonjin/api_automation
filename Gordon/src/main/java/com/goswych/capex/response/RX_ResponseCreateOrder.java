package com.goswych.capex.response;

public class RX_ResponseCreateOrder extends RX_ResponseBase {
    public String  orderId;// = "489686920256";

    @Override
    public String toString(){
        return String.format(
                "{\"orderId\":%s,\"success\":%b,\"message\":\"%s\",\"statusCode\":%d}",
                orderId,success, message, statusCode
                );
    }
}
