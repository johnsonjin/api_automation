package com.goswych.net;

import com.google.gson.Gson;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;
import retrofit2.Response;

/**
 *
 * @param <T>
 */
public class PullRXObserver<T> implements Observer<T> {

	private Thread callerThread;
	private T resp = null;
	private Throwable ex = null;
    private HttpException httpEx = null;

	private Object synObject = null;

    /**
     * request may be not on caller thread and caller is waiting for notify on synObject
     * @param synObject
     */
	public PullRXObserver(Object synObject) {
		callerThread = Thread.currentThread();
		this.synObject = synObject;
	}

    /**
     * run on caller thread
     */
	public PullRXObserver() {
		callerThread = Thread.currentThread();
	}

    /**
     * Provides the Observer with the means of cancelling (disposing) the
     * connection (channel) with the Observable in both
     * synchronous (from within {@link #onNext(Object)}) and asynchronous manner.
     *
     * @param d the Disposable instance whose {@link Disposable#dispose()} can
     * be called anytime to cancel the connection
     * @since 2.0
     */
	@Override
    public void onSubscribe(@NonNull Disposable d) {
        System.out.println("Method will be called when an Observer subscribes to Observable.");
        System.out.println("onSubscribe done");
    }

	/**
	 *  In case of any error, onError() method will be called.
	 */
    @Override
    public void onError(Throwable e) {
        ex = e;
        System.out.println(e);;
        System.out.println("onError: Thread = " + Thread.currentThread().getName());

        if(e instanceof HttpException){
            HttpException ex = ((HttpException) e);
            int statusCode = ex.code();
            Response<?> response = ex.response();
            String msg = ex.message();

            System.out.println("onError: statusCode = " + statusCode);
            System.out.println("onError: response = " + response);
        }

        System.out.println("onError() done");
        //never call not onComplete() if there are any error
        synNotify();
    }
	/**
     *  This method will be called when Observable starts emitting the data.
     *  Retrofit2 all responses with code 2xx will be called from onNext() callback and
     *  the rest of HTTP codes like 4xx, 5xx will be called on the onError() callback,
	 */
    @Override
    public void onNext(T resp) {

    	System.out.println("onNext: Thread = " + Thread.currentThread().getName());

        this.resp = resp;

    	System.out.println("RESPONSE >> " +  resp.getClass().getName());
        String json = new Gson().toJson(resp);
        do{
            if(json.length() > 4000) {
            	System.out.println(">>> RESPONSE " +  json.substring(0, 4000));
                json = json.substring(4000);
            }else if(json.length() ==0) {
                json = null;
            }else{
            	System.out.println(">>> RESPONSE " +  new Gson().toJson(resp));
                json = null;
            }
        }while(json != null);
    }

    /**
     *  When an Observable completes the emission of all the items, onComplete() will be called.
     */
    @Override
    public void onComplete() {
    	System.out.println("onCompleted: Thread = " + Thread.currentThread().getName());
    	System.out.println("onCompleted() done");
        synNotify();
    }

    public T getResp(){
    	return resp;
    }

    public Throwable getThrowable(){
    	return ex;
    }
    public HttpException getHttpEx(){
        return httpEx;
    }

    private void synNotify(){
    	if(callerThread != Thread.currentThread() && synObject != null) {
            synchronized (synObject) {
            	synObject.notify();
            	System.out.println("synObject.notify() done");
            }
    	}
    }
}
