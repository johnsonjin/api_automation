/*
 * https://stackoverflow.com/questions/45978531/okhttp-cookiejar-savefromresponse-method
 */
package com.goswych.net;

import java.util.ArrayList;
import java.util.List;

import okhttp3.CookieJar;
import okhttp3.Cookie;
import okhttp3.HttpUrl;

/**
 * Cookie jar for http request if session required.
 *
 * @author Gordon.Wu
 *
 */
public class OkHttpCookieJar implements CookieJar{
	private final List<Cookie> mAllCookieList = new ArrayList<>();

	@Override
	public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
//		System.out.println("save cookie From Response");
	    for (Cookie cookie : cookies) {
	    	if(!mAllCookieList.contains(cookie)){
		    	mAllCookieList.add(cookie);

//				System.out.println("url = " + url);
//				System.out.println("new cookie Added = " + cookie);
	    	}
	    }
	 }

	@Override
	public List<Cookie> loadForRequest(HttpUrl url) {
//		System.out.println("load cookie For Request");
		List<Cookie> result = new ArrayList<>();
        for (Cookie cookie : mAllCookieList) {
            if (cookie.matches(url)) {
                result.add(cookie);
            }
        }
//		System.out.println("url = " + url);
//		System.out.println("result = " + result);

		return result;
	}
}
