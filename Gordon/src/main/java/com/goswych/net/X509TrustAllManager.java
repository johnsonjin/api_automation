package com.goswych.net;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.X509TrustManager;

/**
 * To accept any certificate, include self-signed ones
 */
public class X509TrustAllManager implements X509TrustManager {

    private List<X509Certificate> certList = new ArrayList<X509Certificate>();

    @Override
    public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        for (X509Certificate x509Certificate : chain) {
            this.certList.add(x509Certificate);
        }
    }

    @Override
    public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        for (X509Certificate x509Certificate : chain) {
            this.certList.add(x509Certificate);
        }
    }

    @Override
    public X509Certificate[] getAcceptedIssuers() {
        return this.certList.toArray(new X509Certificate[this.certList.size()]);
    }

    private X509Certificate getX509Certificate() {
        X509Certificate x509Certificate = null;
        if(!certList.isEmpty()){
            x509Certificate = certList.get(certList.size()-1);
        }
        return x509Certificate;
    }
}
