package com.goswych.net;


import retrofit2.HttpException;
import retrofit2.Response;

public class RxException  extends RuntimeException {
    private Throwable ex;
    private HttpException httpEx = null;
    private final transient Response<?> response ;

    public RxException(Throwable ex) {
        super();
        this.ex = ex;
        if (ex instanceof HttpException){
            httpEx = (HttpException)ex;
            response = httpEx.response();
        }else{
            response = null;
        }
    }

    /** HTTP status code if Throwable ex is HttpException otherwise -1 */
    public int code() {
        if (httpEx != null){
            return httpEx.code();
        }
        return -1;
    }

    /**
     * Throwable message.
     */
    public String message() {
        return ex.getMessage();
    }

    /**
     * The full HTTP response. This may be null
     */
    public Response<?> response() {
        return response;
    }

}
