package com.goswych.net;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RestServiceFactory {


    public static <T> T createService( Class<T>  serviceClass, String restURL, boolean acceptAllCertificate)
            throws NoSuchAlgorithmException, KeyManagementException {

        OkHttpClient.Builder okHttpClientBuilder = null;
        okHttpClientBuilder = RestServiceFactory.buildOkHttpClientBuilder(acceptAllCertificate);

        okHttpClientBuilder.writeTimeout(10000, TimeUnit.SECONDS); // connect timeout
        okHttpClientBuilder.readTimeout(10000, TimeUnit.SECONDS); // connect timeout
        okHttpClientBuilder.connectTimeout(10000, TimeUnit.SECONDS); // connect timeout

        OkHttpClient okHttpClient = okHttpClientBuilder.build();

        // see GitHubService for URL
        final Gson gson = new GsonBuilder().create();

        if(!restURL.endsWith("/")){
            //API_BASE_URL should end with "/", so append it now
            restURL += "/";
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(restURL)
                // for cookie management
                .client(okHttpClient)

                //Retrofit2 Call<ApplicationResp> to rxJava Observable<ApplicationResp>
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

                //Gson response
                .addConverterFactory(GsonConverterFactory.create(gson))

                //compile 'com.squareup.retrofit2:converter-scalars:2.1.0'
                .addConverterFactory(ScalarsConverterFactory.create())	//	string response
                //another way: return Call<ResponseBody> --> in Callback: response.body().string()

                .build();

        return retrofit.create(serviceClass);

    }

    /**
     *
     * @param filePassword
     * A password may be given to unlock the keystore
     * (e.g. the keystore resides on a hardware token device),
     * or to check the integrity of the keystore data.
     * If a password is not given for integrity checking,
     * then integrity checking is not performed.
     *
     * @param keystoreInputStream:
     * 	InputStream such as context.getResources().openRawResource(R.raw.keystoreFileId) or
     * 	FileInputStream such as new java.io.FileInputStream("keyStoreFile");
     *
     * @return KeyStore
     *
     * @throws KeyStoreException
     * @throws FileNotFoundException
     * @throws IOException
     * @throws NoSuchAlgorithmException
     * @throws CertificateException
     */
    public static KeyStore readKeyStore(String filePassword,
                                        InputStream keystoreInputStream) throws KeyStoreException,
            FileNotFoundException, IOException, NoSuchAlgorithmException, CertificateException{

        //KeyStoreException
        KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
        //IOException, NoSuchAlgorithmException, CertificateException
        ks.load(keystoreInputStream, filePassword.toCharArray());

        return ks;
    }

    /**
     * @param keystoreInputStream:
     * 	InputStream such as context.getResources().openRawResource(R.raw.keystoreFileId) or
     * 	FileInputStream such as new java.io.FileInputStream("keyStoreFile");
     *
     * @param storePassword
     * @param keystoreInputStream
     * @return
     * @throws
     */
    public static SSLContext buildSSLConextSelfSigned(String filePassword,
                                                      String storePassword,
                                                      InputStream keystoreInputStream)
            throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException,
            UnrecoverableKeyException, KeyManagementException {
        SSLContext sslContext = null;
        KeyStore keyStore = readKeyStore(filePassword, keystoreInputStream);

        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        trustManagerFactory.init(keyStore);

        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        keyManagerFactory.init(keyStore, storePassword.toCharArray());

        sslContext = SSLContext.getInstance("SSL");
        sslContext.init(keyManagerFactory.getKeyManagers(),trustManagerFactory.getTrustManagers(), new SecureRandom());

        return sslContext;
    }

    /**
     *
     * @return
     * @throws
     */
    public static SSLContext buildSSLConextTrustAll() throws
            NoSuchAlgorithmException, KeyManagementException {
        SSLContext sslContext = null;

        TrustManager[] myTrustManagerArray = new TrustManager[]{new X509TrustAllManager()};

        sslContext = SSLContext.getInstance("SSL");
        sslContext.init(null, myTrustManagerArray, new SecureRandom());
        //java.lang.IllegalStateException: KeyManagerFactoryImpl is not initialized

        return sslContext;
    }
    public static OkHttpClient.Builder buildOkHttpClientBuilder(String filePassword,
                                                                String storePassword,
                                                                File keystoreFile) throws Exception {
        OkHttpClient.Builder okHttpClientBuilder = buildOkHttpClientBuilder(false);
		/*
			https://stackoverflow.com/questions/23103174/does-okhttp-support-accepting-self-signed-ssl-certs
			SSLContext sslContext = SSLContext.getInstance("SSL");
		 */
        if(keystoreFile != null && keystoreFile.exists()){
            InputStream keystoreInputStream =  new FileInputStream(keystoreFile);
            SSLContext sslContext = buildSSLConextSelfSigned(filePassword, storePassword, keystoreInputStream);
            okHttpClientBuilder.sslSocketFactory(sslContext.getSocketFactory());
        }

        return okHttpClientBuilder;
    }

    /**
     * @param acceptAllCertificate
     * @return OkHttpClient.Builder
     * @throws
     */
    public static OkHttpClient.Builder buildOkHttpClientBuilder(boolean acceptAllCertificate)
            throws KeyManagementException, NoSuchAlgorithmException {
        OkHttpCookieJar cookieJar = new OkHttpCookieJar();
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient().newBuilder().
                cookieJar(cookieJar)
                .connectTimeout(60 * 5, TimeUnit.SECONDS)
                .readTimeout(60 * 5, TimeUnit.SECONDS)
                .writeTimeout(60 * 5, TimeUnit.SECONDS);

        if(acceptAllCertificate){
			/*
				https://stackoverflow.com/questions/23103174/does-okhttp-support-accepting-self-signed-ssl-certs
				SSLContext sslContext = SSLContext.getInstance("SSL");
			 */
            SSLContext sslContext = buildSSLConextTrustAll();
            okHttpClientBuilder.sslSocketFactory(sslContext.getSocketFactory());
        }
        return okHttpClientBuilder;
    }

}
